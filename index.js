import PKG from "./package.json"
import Block from "./src/blockchain/block";

const {name, version} = PKG;


const {genesis} = Block;
// const block = new Block(Date.now(), 'b', 'c', 'd')
const block1 = Block.mine(genesis, 'dato 1')
console.log(block1.toString())
const block2 = Block.mine(block1, 'dato 2')
console.log(block2.toString())
