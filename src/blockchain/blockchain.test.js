import Blockchain from "./blockchain";
import Block from "./block";
import {beforeEach, describe, expect, it} from "@jest/globals";

describe('Blockchain', () => {
  let blockchain;
  let blockchainB;
  beforeEach(() => {
    blockchain = new Blockchain();
    blockchainB = new Blockchain();
  })
  it('toda blockchain tiene un genesis block', () => {
    const [genesisBlock] = blockchain.blocks;
    expect(genesisBlock).toEqual(Block.genesis)
    expect(blockchain.blocks.length).toEqual(1)
  })

  it('addBlock()', () => {
    const data = 'data'
    blockchain.addBlock(data)
    const [, lastBlock] = blockchain.blocks;
    expect(lastBlock.data).toEqual(data)
    expect(blockchain.blocks.length).toEqual(2)
  })

  it('replace the chain', function () {
    blockchainB.addBlock('data');
    blockchain.replace(blockchainB.blocks)
    expect(blockchain.blocks).toEqual(blockchainB.blocks)
  });

  it(' does not replace the chain', function () {
    blockchain.addBlock('data');
    blockchain.replace(blockchainB.blocks)
    expect(() => {
      blockchain.replace(blockchainB.blocks)
    }).toThrowError('Received chain no valid')
  });

  it('not replace in chain with one is valid', function () {
    blockchainB.addBlock('data');
    blockchainB.blocks[1].data = 'block-hack'
    expect(() => {
      blockchain.replace(blockchainB.blocks)
    }).toThrowError('Received chain in valid')
  });
})
