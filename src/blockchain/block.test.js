import Block, {DIFFICULTY} from "./block";
import {beforeEach, describe, expect, it} from "@jest/globals";

describe('Block', () => {
  let timestamp;
  let previousBlock;
  let hash;
  let data;
  let nonce;
  beforeEach(() => {
    timestamp = new Date(2010, 0, 1);
    previousBlock = Block.genesis;
    data = 'testdata'
    hash = 'hash'
    nonce = 128;
  })
  it('create an instance with params ', function () {
    const block = new Block(timestamp, previousBlock.hash, hash, data, nonce)
    expect(block.previousHash).toEqual(previousBlock.hash)
    expect(block.hash).toEqual(hash)
    expect(block.data).toEqual(data)
    expect(block.nonce).toEqual(nonce)
  });

  it('use mine() ', function () {
    const block = Block.mine(previousBlock, data)
    const {difficulty} = block;
    expect(block.hash.length).toEqual(64)
    expect(block.hash.substring(0, difficulty)).toEqual('0'.repeat(difficulty))
    expect(block.previousHash).toEqual(previousBlock.hash)
    expect(block.nonce).not.toEqual(0)
    expect(data).toEqual(data);
  });
  it('use hash() ', function () {
    const hash = Block.hash(timestamp, previousBlock.hash, data, nonce)
    const keyOutput = 'c6dab7c21abe4fc06e0c8dd0888c36992d920540b2c9784d041400047aa76e45';
    expect(hash).toEqual(keyOutput)
  });

  it('use toString() ', function () {
    const block = Block.mine(previousBlock, data)
    console.log(block.toString())
    expect(typeof block.toString()).toEqual('string')
  });
})
