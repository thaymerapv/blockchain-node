import MemoryPool from "./memoryPool";
import Transaction from "../wallet/transaction";
import Wallet from "../wallet/wallet";
import {beforeEach, describe, expect, it} from "@jest/globals";

describe('Memory Poll', () => {
  let memoryPool;
  let transaction;
  let wallet;
  beforeEach(() => {
    memoryPool = new MemoryPool();
    wallet = new Wallet();
    transaction = Transaction.create(wallet, 'random Address', 5)
    memoryPool.addOrUpdate(transaction)
  })

  it('has one transaction', function () {
    expect(memoryPool.transactions.length).toEqual(1)
  });

  it('add transaction in memoryPool', function () {
    const found = memoryPool.transactions.find(({id}) => id === transaction.id)
    expect(found).toEqual(transaction)
  });

  it('update transaction in memoryPool', function () {
    const txOld = JSON.stringify(transaction);
    const txNew = transaction.update(wallet, 'other address', 10)
    memoryPool.addOrUpdate(txNew)
    const found = memoryPool.transactions.find(({id}) => id === transaction.id)
    expect(JSON.stringify(found)).not.toEqual(txOld)
    expect(txNew).toEqual(found)
  });
})
