import adjustDifficulty from "./adjustDifficulty";
import {beforeEach, describe, expect, it} from "@jest/globals";

describe('adjustDifficulty', () => {
  let block;
  beforeEach(() => {
    block = {timestamp: Date.now(), difficulty: 3}
  })

  it('for slowly mined blocks ', function () {
    expect(adjustDifficulty(block, block.timestamp + 60000)).toEqual(block.difficulty - 1)
  });
  it('for quick mined blocks ', function () {
    expect(adjustDifficulty(block, block.timestamp + 1000)).toEqual(block.difficulty + 1)
  });
})
