import Block from "../block";

export default (blockchain) => {
  const {genesisBlock, ...blocks} = blockchain;
  if (JSON.stringify(genesisBlock) !== JSON.stringify(Block.genesis)) new Error('invalid genesis Block');
  for (let i = 0; i < blocks.length; i++) {
    const {
      previousHash, timestamp, hash, data, nonce,difficulty
    } = blocks[i]
    const previousBlock = blockchain[i];
    if (previousHash !== previousBlock.hash)throw new Error('invalid previous hash')
    if (hash !== Block.hash(timestamp, previousHash, data, nonce,difficulty))throw new Error('Invalid Hash')
  }
  return true
}
