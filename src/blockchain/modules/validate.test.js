import Blockchain from "../blockchain";
import validate from "./validate";
import {beforeEach, describe, expect, it} from "@jest/globals";

describe('validate()', () => {
  let blockchain;
  beforeEach(() => {
    blockchain = new Blockchain()
  })
  it('valiate in valid chain', () => {
    blockchain.addBlock('block-1')
    blockchain.addBlock('block-2')
    expect(validate(blockchain.blocks)).toBe(true)
  })

  it('invalidates genesis block', () => {
    blockchain.blocks[0].data = 'soy un invalido'
    expect(() => {
      validate(blockchain.blocks);
    }).toThrowError('invalidates genesis block')
  })

  it('invalidates hash in chain', () => {
    blockchain.addBlock('block-1');
    blockchain.blocks[1].previousHash = 'i dont now'
    expect(() => {
      validate(blockchain.blocks);
    }).toThrowError('invalid previous hash')
  })

  it('invalidates corrupt hash in chain', () => {
    blockchain.addBlock('block-1');
    blockchain.blocks[1].previousHash = 'i dont now 2'
    expect(() => {
      validate(blockchain.blocks);
    }).toThrowError('invalid hash')
  })
})
