import Wallet, {INITIAL_BALANCE} from "./wallet";
import Blockchain from "../blockchain";
import {beforeEach, describe, expect, it} from "@jest/globals";

describe('Wallet', () => {
  let blockchain;
  let wallet;
  beforeEach(() => {
    blockchain = new Blockchain();
    wallet = new Wallet(blockchain);
  })
  it('is is a healthy wallat ', function () {
    expect(wallet.balance).toEqual(INITIAL_BALANCE)
    expect(typeof wallet.keyPair).toEqual('object');
    expect(typeof wallet.publicKey).toEqual('string');
    expect(wallet.publicKey.length).toEqual(130)
  });

  it('user sign() ', function () {
    const signature = wallet.sign('hello')
    expect(typeof signature).toEqual("object")
    expect(signature).toEqual(wallet.sign('hello'))
  });

  describe('create transaction ', () => {
    let tx;
    let recipientAddress;
    let amount;
    beforeEach(() => {
      recipientAddress = 'random address'
      amount = 5;
      tx = wallet.createTransaction(recipientAddress, amount)
    })
    describe('and doing the same transactio', () => {
      beforeEach(() => {
        tx = wallet.createTransaction(recipientAddress, amount)
      })
      it('doble the amount subtracted from the wallet balance ', function () {
        const output = tx.outputs.find(({address}) => address === wallet.publicKey)
        expect(output.amount).toEqual(wallet.balance - (amount * 2))
      });

      it('clones amount output for de recipients ', function () {
        const amounts = tx.outputs.filter(({address}) => address === recipientAddress).map((output) => output.amount);
        expect(amounts).toEqual([amount, amount])
      });
    })
  });
})
