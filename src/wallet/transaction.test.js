import Transaction from "./transaction";
import Wallet from "./wallet";
import {beforeEach, describe, expect, it} from "@jest/globals";

describe('transaccion', () => {
  let amount;
  let wallet;
  let transaction;
  let recipientAddress;
  beforeEach(() => {
    wallet = new Wallet();
    recipientAddress = 'recipient';
    amount = 5
    transaction = Transaction.create(wallet, recipientAddress, amount)
    console.log(transaction)
  })

  it('outputs the amount', function () {
    const output = transaction.outputs.find(({address}) => address === wallet.publicKey)
    expect(output.amount).toEqual(wallet.balance - amount)
  });

  it('output the amount to the recipient ', function () {
    const output = transaction.outputs.find(({address}) => address === recipientAddress)
    expect(output.amount).toEqual(amount)
  });

  describe('transacting with amount that exceeds the balance', () => {
    beforeEach(() => {
      amount = 500;
      transaction = undefined;
    })

    it('does not create transaction ', function () {
      expect(() => {
        transaction = Transaction.create(wallet, recipientAddress, amount)
      }).toThrowError(`Amount: ${amount} exceed balance`)
    });
  })

  it('inputs the balance of the wallet', function () {
    console.log(JSON.stringify(transaction.input))
    expect(transaction.input.amount).toEqual(wallet.balance)
  });

  it('inputs the sender address of the wallet ', function () {
    expect(transaction.input.address).toEqual(wallet.publicKey)
  });

  it('input hash a signature using the wallet', function () {
    expect(typeof transaction.input.signature).toEqual('object')
    expect(transaction.input.signature).toEqual(wallet.sign(transaction.outputs))
  });

  it('validate a transaction ', function () {
    expect(Transaction.verify(transaction)).toBe(true)
  });

  it('invalite corrupt transaction ', function () {
    transaction.outputs[0].amount = 500;
    expect(Transaction.verify(transaction)).toBe(false)
  });

  describe('and update a transaction', () => {
    let nextAmount;
    let nextRecipient;
    beforeEach(() => {
      nextAmount = 3;
      nextRecipient = 'next-address';
      transaction.update(wallet, nextRecipient, nextAmount)
    })

    it('substract the next amount from the sendeer wallet ', function () {
      const output = transaction.outputs.find(({address}) => address === wallet.publicKey)
      expect(output.amount).toEqual(wallet.balance - amount - nextAmount)
    });

    it('outputs an amount for the next recipient ', function () {
      const output = transaction.outputs.find(({address}) => address === nextRecipient)
      expect(output.amount).toEqual(nextAmount)
    });
  })
})
