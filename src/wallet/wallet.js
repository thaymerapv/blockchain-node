import {elliptic, hash} from '../utils'
import Transaction from "./transaction";

const INITIAL_BALANCE = 100;

class Wallet {
  constructor(blockchain) {
    this.balance = INITIAL_BALANCE;
    this.keyPair = elliptic.createKeyPair();
    this.publicKey = this.keyPair.getPublic().encode('hex');
    this.blockchain = blockchain;
  }

  toString() {
    const {balance, feyPair, publicKey} = this;
    return `wallet -
      prvKey   :${feyPair}
      publicKey   :${publicKey}
      balance:    :${balance}
    `;
  }

  sign(data) {
    return this.keyPair.sign(hash(data))
  }

  createTransaction(recipientAddress, amount) {
    const {balance, blockchain: {memoryPool}} = this;
    if (amount > balance) throw Error(`amount: ${amount} excedes`)
    console.log(this.publicKey)
    let tx = memoryPool.find(this.publicKey)
    console.log(tx)
    if (tx) {
      tx.update(this, recipientAddress, amount)
      console.log(tx)
    } else {
      tx = Transaction.create(this, recipientAddress, amount)
      memoryPool.addOrUpdate(tx)
      console.log(tx)
    }

    return tx;
  }
}

export {INITIAL_BALANCE};
export default Wallet;
