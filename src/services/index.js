import express from 'express';
import bodyParser from 'body-parser';
import Blockchain from "../blockchain";
import P2PService from "./p2p";
import Wallet from "../wallet/wallet";

const {HTTP_PORT = 3000} = process.env;
const app = express();
const blockchain = new Blockchain();
const wallet = new Wallet(blockchain)
const p2pService = new P2PService(blockchain);
blockchain.addBlock('express')
app.use(bodyParser.json())

app.get('/blocks', (req, res) => {
  try {
    return res.status(200).json(blockchain.blocks);
  } catch (err) {
    return res.status(200).json({ok: false, message: err.message})
  }
})

app.post('/mine', (req, res) => {
  try {
    const {data} = req.body;
    const block = blockchain.addBlock(data)
    p2pService.sync();
    return res.status(200).json({blocks: blockchain.blocks.length, block});
  } catch (err) {
    return res.status(500).json({ok: false, message: err.message})
  }
})
app.get('/transactions', (req, res) => {
  const {memoryPool: {transactions}} = blockchain
  return res.status(200).json({ok: true, transactions})
})
app.post('/transactions', (req, res) => {
  const {recipient, amount} = req.body;
  try {
    const tx = wallet.createTransaction(recipient, amount)
    return res.status(200).json(tx)
  } catch (err) {
    return res.status(500).json({ok: false, message: err.message})
  }
})
app.listen(HTTP_PORT, () => {
  console.log(`server on port : ${HTTP_PORT}`)
  p2pService.listen();
})
